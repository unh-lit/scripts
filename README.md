A collection of scripts that comprise components of the UNH DigitalCollections software. 

See the [Fedora project documentation wiki](https://gitlab.com/unh-lit/hyrax516/-/wikis/home) for documentation.
