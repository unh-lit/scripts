#!/bin/bash
#
# Sample command lines:
# ./export-collections.sh ./fedora3-collections-a.txt
# - OR -
# ./export-collections.sh ./fedora3-collections-a.txt > output-a.txt 2>&1 &
# tail -f output-a.txt
#
# Where fedora3-collections-a.txt lists a collection name on each line (#=line is commented out):
#
# #acworth
# acworth
# adjutant
# agbulletin
# ...
#
grep -v '^#' < "$1" | { while read line; do ./f3export.sh -q "pid~${line}:*" -f -d "/cygdrive/d/FedoraStorage/collections/${line}" -o "${line}.csv" ; done; }
# grep -v '^#' < "$1" | { while read line; do ./f3export.sh -q "pid~${line}:*" -f -d "/Volumes/FedoraStorage/collections/${line}" -o "${line}.csv" ; done; }
# grep -v '^#' < "$1" | { while read line; do ./f3export.sh -q "pid~${line}:*" -f -d "./tmp/collections/${line}" -o "${line}.csv" ; done; }
