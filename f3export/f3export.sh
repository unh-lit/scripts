#!/usr/bin/bash

#
# DOCUMENTATION:
# This script will pull specified objects from the Fedora 3 digital repository at digital.unh.edu.  The object's metadata will be
# dumped into a csv file.  Optionally, any files associated with that object will also be downloaded.  By default, the csv file
# will be deposited in ./tmp/out.csv.  Also any downloaded files will deposited into ./tmp by default.
#
# With no arguments, this script will try to retrieve all objects from the Fedora 3 Repository at digital.unh.edu
# and dump their metadata into ./tmp/out.csv.  This is not the recommended way of doing it.  It could take several days
# to run, so it is recommended to use -q to selectively dump objects instead.
#
# As long as the requirements are met, this script can be run from the Linux command line from any computer within the UNH VPN.
# This script can also be run from the Mac OS X terminal window or from a CygWin terminal on Windows.
#
# Add some documentation for the 'test_fix' branch.
# Add some documentation for the '5-test-title-for-issue' branch.
#
# SCRIPT REQUIREMENTS:
#   - Tidy must be installed and in your search paths. ('yum install tidy' on linux or 'brew install tidy' on mac os x)
#   - Bash >=v4.0 must be installed and must be your current shell.
#   - Curl must be installed and in your search paths. ('yum install curl' on linux or 'brew install curl' on mac os x)
#   - Xmlstarlet must be installed and in your search paths (add 'extras' RHEL repo then 'yum install xmlstarlet' on linux
#     or 'brew install xmlstarlet' on mac os x)
#   - Output_dir_path must exist and the user must have 'w' access.
#   - On Windows, Cygwin must be installed prior to installing other required software.  The other required packages can
#     be installed at the same time CygWin is installed.
#   - On Mac OS, Brew is the suggested way of installing other required software.
#   - On Linux, yum can be used to installed required software.
#
# SETUP:
#   - Set up all script requirements.
#
#   - Clone the gitlab repo for this script:
#
#       git clone git@gitlab.com:unh-lit/scripts.git
#
#		cd scripts/f3export
#
#   - Run the command as describe in 'USAGE'.
#
# NOTES:
#
# Run this script from its root directory so it has access to the tmp directory for outputting the CSV file.
#
# Files are not downloaded by default. Only the csv file is generated.  To download files, use the -f option.
#
# This script runs very SLOWLY, so be patient.  However, you should see output pretty much right away.
#
# USAGE:
#
#   ./f3export.sh -h
#
#   ./f3export.sh
#
#   ./f3export.sh [-q query_string] [-o output_file_name] [-d output_dir_path] [-f | -e] [-v [0-2+]]
#
#   ./f3export.sh [-q query_string] -r
#
#
# OPTIONS:
#   -h
#       Prints a usage message and quits.
#
#   -a
#       Instead of overwriting the .csv file, appends.  This will cause the header (first line with column names) to be skipped.
#
#   -d output_dir_path
#       Directory to which object files are exported, including the out.csv file
#       Currently only files with mimetype tiff, jpeg, pdf are exported. This can be expanded. (Default: ./tmp.)
#
#   -e
#       (Coming soon.) Expedient csv dump.  No files are downloaded, and no checking is done for them on the Fedora server.
#
# 	-f
#		Turns on file downloading. With this option, if a fedora object has files associated with it, they are downloaded
#		to the output_dir_path (-d). (Default: off.)
#
#   -o output_file_name
#       The csv output file path. (Default: out.csv.)
#
#   -q query_string
#       A query to specify which objects to extract from the Fedora 3 repository.  The default is no query which will pull
#       all objects from the Fedora 3 repository, 20 at a time.  This can take quite a long time.
#       A good description of allowable queries is at http://digital.unh.edu:8080/fedora/objects, click on either of the 'help' links.
#       (Default: no query string which means 'pull all objects from the repository'.)
#
#   -v verbosity level
#       How much output you see from the script.  0 = silent.  1 = some output (default). 2+ = verbose (debug).
#
#   -r
#       (Coming soon.) Run in reporting mode. Only audit trail reporting for now.
#
# Informative URLS are:
#   - http://digital.unh.edu:8080/fedora/describe
#   - http://digital.unh.edu:8080/fedora/objects
#
# Currently this script only accesses the Fedora 3 repository at http://digital.unh.edu:8080, but
# arguments will be added to allow this to access any Fedora 3 repository given a domain, port, and Fedora base (default is 'fedora').
#
# TO DO:
#   - Check that all requirements are met before executing the script.
#   - Move UNAME/PW to '.config' file for security purposes.
#   - Ask about whether to pick up DC datastream elements.
#   - Add -r argument (to generate report on the audit properties.) (*)
#   - Argument (getopts) processing needs more work to diagnose invalid cases.
#   - Parameterize domain and port (digital.unh.edu:8080) for curl calls
#   - Check for unprocessed datastreams with mimetypes we don't check for - just report on them. (*)
#
# SAMPLE COMMANDS:
# ./f3export.sh -h
# ./f3export.sh -q "pid~a*"
# ./f3export.sh -q "pid~demo*"
# ./f3export.sh -q "pid~moran:0195"
# ./f3export.sh -q "pid~acworth:0001"
# ./f3export.sh -q "pid~acworth:000*"
# ./f3export.sh -q "pid~demo:5"
# ./f3export.sh -q "pid~moran:0195" -o output_file_name -d output_dir_path -f
# ./f3export.sh -q "pid~morann:0195"
# ./f3export.sh -q "pid~acworth:*" -f -d ./tmp/fedora3-data-exports/acworth
# ./f3export.sh -q "pid~demo:SmileyTallRoundCup" -f
#
# Example - put a number of the following lines into a file, 'export-a.sh'.
#      Note the 1st command has no '-a' so that the acworth.csv file is emptied and has a header applied.
#      Note the 2nd and all following lines have -a applied.
#
# #!/usr/bin/bash
# ./f3export.sh -q "pid~acworth:0001" -f -d "/cygdrive/d/FedoraStorage/collections/acworth" -o "acworth.csv"
# ./f3export.sh -q "pid~acworth:0002" -f -d "/cygdrive/d/FedoraStorage/collections/acworth" -o "acworth.csv" -a
# ./f3export.sh -q "pid~acworth:0003" -f -d "/cygdrive/d/FedoraStorage/collections/acworth" -o "acworth.csv" -a
# ./f3export.sh -q "pid~acworth:0004" -f -d "/cygdrive/d/FedoraStorage/collections/acworth" -o "acworth.csv" -a
# ./f3export.sh -q "pid~acworth:0005" -f -d "/cygdrive/d/FedoraStorage/collections/acworth" -o "acworth.csv" -a
#
# The run the 'export-a.sh file as below.  This runs the command in the background and also
# puts all command output into 'output.a' which can be followed using the tail command
#
# ./export-acworth.sh >> output-a.txt 2>&1 &
# tail -f output-a.txt
#

#
# ADDITIONAL DOCUMENTATION: SAMPLES
# GETREQUESTBASE='http://digital.unh.edu:8080/fedora/objects?terms&maxResults=20&resultFormat=xml&pid=true&title=true&identifier=true&query'
# RESPONSE=$(curl --location --request GET "http://digital.unh.edu:8080/fedora/objects?terms&maxResults=20&resultFormat=xml&pid=true&title=true&identifier=true&query&sessionToken=$TOKEN")
# curl -u 'fedoraAdmin:D1G!7@l'  -ILs 'http://digital.unh.edu:8080/fedora/objects/demo:5/datastreams/HIGHRES_IMG/content'
# curl -u 'fedoraAdmin:D1G!7@l'  -ILs 'http://digital.unh.edu:8080/fedora/objects/demo:5/datastreams/HIGHRES_IMG/content?download=true' | grep -i '^HTTP' | awk '{print $2}'

UNAME="fedoraAdmin"
PW="D1G!7@l"

OUTFILE='out.csv'
FILEDIR='./tmp'
TOKENARG='&sessionToken='
TOKEN=''
FILESARG=0
VERBOSEARG=1
QUERYARG=''
EXPEDIENTARG=0
#GETREQUESTBASE='http://digital.unh.edu:8080/fedora/objects?terms&maxResults=20&resultFormat=xml&pid=true&title=true&identifier=true'
#GETREQUESTBASE='http://digital.unh.edu:8080/fedora/objects?terms&maxResults=5&resultFormat=xml&pid=true&title=true&identifier=true'
GETREQUESTBASE='http://digital.unh.edu:8080/fedora/objects?terms&maxResults=2&resultFormat=xml&pid=true&title=true&identifier=true'
#QUERY="query"
SEPARATOR='|~|'
SEPARATORLEN=size=${#SEPARATOR}
STATE=''
APPENDARG=0

TOTDOWNLOADS=0
POSSDOWNLOADS=0
SKIPPEDDOWNLOADS=0
declare -A DOWNLOADERRS

declare -a DCQCONTRIBUTORS
declare -a DCQCOVERAGES
declare -a DCQCREATORS
declare -a DCQDATES
declare -a DCQDESCRIPTIONS
declare -a DCQFORMATS
declare -a DCQIDENTIFIERS
declare -a DCQLANGUAGES
declare -a DCQPUBLISHERS
declare -a DCQRELATIONS
declare -a DCQRIGHTS
declare -a DCQSUBJECTS
declare -a DCQTITLES
declare -a DCQTYPES

declare -a DCCREATORS
declare -a DCDESCRIPTIONS
declare -a DCPUBLISHERS
declare -a DCIDENTIFIERS
declare -a DCSUBJECTS
declare -a DCTITLES

declare -a VALUES

declare -a COLLECTIONS

# FOR XMLSTARLET - NAMESPACES
declare -A NAMESPACES=( [FOXML]="info:fedora/fedora-system:def/foxml#" [DC]="http://purl.org/dc/elements/1.1/" [RDF]="http://www.w3.org/1999/02/22-rdf-syntax-ns#" [REL]="info:fedora/fedora-system:def/relations-external#" [FEDORAMODEL]="fedora/fedora-system:def/model#" [DCTERMS]="http://purl.org/dc/terms/")


####################### FUNCTIONS - API CALLS #######################
#
# GIVEN:
#   - The PID of a Fedora 3 object.
#
# RETURNS:
#   - The foxml node describing that object in Fedora 3, if there is one. Otherwise, returns an empty string.
#
# DESCRIPTION:
# Performs a Fedora 3 API request to get foxml node for a Fedora 3 object with the given PID.
#
function f3api_objectXML()
{
    local myresult=''
    # local request_base='http://digital.unh.edu:8080/fedora/objects/%s/objectXML'
    local request=''

    # request=$(printf "${request_base}" "$1")
    request=$(printf "http://digital.unh.edu:8080/fedora/objects/%s/objectXML" "$1")

    # set -x
    myresult=$(curl -s --user ${UNAME}:${PW} "${request}")
    # set +x

    echo "$myresult"
}

#
# GIVEN:
#   - The PID of a fedora object.
#   - The ID of a datastream object.
#   - The filename of the file represented by the datastream object.
#   - The path of a directory naming where to put the downloaded file.
#
# RETURNS:
#    - The result of the curl request used to download the file.
#
# DESCRIPTION:
# Performs a Fedora 3 API request to download a file from an object with PID $1, DATASTREAM_ID $2, FILENAME $3, FILEDIR $4 (directory to put file)
#
function f3api_objectContent()
{
    local myresult=''
    # local request_base='http://digital.unh.edu:8080/fedora/objects/%s/datastreams/%s/content?download=true'
    local request=''

    # request=$(printf "${request_base}" "$1" "$2")
    request=$(printf "http://digital.unh.edu:8080/fedora/objects/%s/datastreams/%s/content?download=true" "$1" "$2")

    # set -x
    myresult=$(curl -s --user ${UNAME}:${PW} "${request}" --output "${4}/${3}")
    # set +x

    echo "$myresult"
}

#
# GIVEN:
#   - The PID of a fedora object.
#   - The ID of a datastream object.
#
# RETURNS:
#    - The resulting http code of the 'file download' call
#
# DESCRIPTION:
# Performs a Fedora 3 API request to download a file from an object with PID $1, DATASTREAM_ID $2, FILENAME $3, FILEDIR $4 (directory to put file)
#
function f3api_objectCode()
{
    local myresult=''
    # local request_base='http://digital.unh.edu:8080/fedora/objects/%s/datastreams/%s/content'
    local request=''

    # request=$(printf "${request_base}" "$1" "$2")
    request=$(printf "http://digital.unh.edu:8080/fedora/objects/%s/datastreams/%s/content" "$1" "$2")

    # set -x
    myresult=$(curl -s --user ${UNAME}:${PW} -ILs "${request}" | grep -i '^HTTP' | awk '{print $2}' )
    # set +x

    echo "$myresult"
}

####################### FUNCTIONS - EXTRACT DATA FROM AN OBJECT'S FOXML #######################
#
# THIS IS FOR THE NEXT FEW FUNCTIONS!!
#
# GIVEN:
#   - The foxml of a Fedora 3 object - the complete document for an object with a given PID.
#   - A datastream ID.
#
# RETURNS:
#   - The mimetype/label/internal_id/type/url of the named datastream.
#
# DESCRIPTION:
# Extracts the specified quantity the specified datastream (ID) contained in the foxml of a specified object (PID).
#
# These functions are called for 'content' datastreams (files).
#
# NOTES:
#   - We are assuming the datastream is versionable and picking the last datastreamVersion element (hopefully the latest).
#   - Data extracted is raw.  Processing to escape characters for csv file happens outside of these functions.
#
function foxmlExtract_ContentMimeType()
{
    local myresult=''

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]/@MIMETYPE" - )

    echo "${myresult}"
}


function foxmlExtract_ContentLabel()
{
    local myresult=''

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]/@LABEL" - )

    echo "${myresult}"
}


function foxmlExtract_ContentInternalID()
{
    local myresult=''

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]/foxml:contentLocation[@TYPE='INTERNAL_ID']/@REF" - )

    echo "${myresult}"
}


function foxmlExtract_ContentType()
{
    local myresult=''

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]/foxml:contentLocation/@TYPE" - )

    echo "${myresult}"
}


function foxmlExtract_ContentUrl()
{
    local myresult=''

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]/foxml:contentLocation/@REF" - )

    echo "${myresult}"
}

#
# THIS IS FOR THE NEXT FEW FUNCTIONS!!
#
# GIVEN:
#   - The foxml of a Fedora 3 object - the complete document for an object with a given PID.
#
# RETURNS:
#   - The value of specified quantities from the 'RELS_EXT' datastream.
#
# DESCRIPTION:
# Extracts a specified quantity from the datastream (ID=RELS_EXT) contained in the foxml of a fedora 3 object (PID).
#
function foxmlExtract_Collections()
{
    local myresult=''
    local itemsCount=0
    local i

    VALUES=()

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -N rdf="${NAMESPACES[RDF]}" -N rel="${NAMESPACES[REL]}" -t -v "//foxml:datastream[@ID='RELS-EXT']/foxml:datastreamVersion[last()]//rdf:Description//rel:isMemberOfCollection/@rdf:resource" - )
	itemsCount=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -N rdf="${NAMESPACES[RDF]}" -N rel="${NAMESPACES[REL]}" -t -v "count(//foxml:datastream[@ID='RELS-EXT']/foxml:datastreamVersion[last()]//rdf:Description//rel:isMemberOfCollection/@rdf:resource)" - )

	if (( itemsCount != 0)) ;
	then
	    readarray -t VALUES <<<"$myresult"

		for i in "${!VALUES[@]}"; do
  			VALUES[$i]="${VALUES[$i]##*:}"
		done
	fi

    (( v >= 2 )) && printf "%s\n" "COLLECTIONS:"
    (( v >= 2 )) && printf "%s\n" "MYRESULT = ${myresult}"
    (( v >= 2 )) && printf "%s\n" "itemsCount = ${itemsCount}"
    (( v >= 2 )) && printf '%s\n' "${VALUES[@]}"
}

#
# GIVEN:
#   - The foxml of a Fedora 3 object - the complete document for an object with a given PID.
#   - The datastream name.
#   - The term name.
#
# RETURNS:
#   - An array of the values
#
# DESCRIPTION:
# Extracts a specified quantity from the datastream (ID=??) contained in the foxml of a fedora 3 object (PID).
#
function foxmlExtract_Values()
{
    local myresult=''
    local itemsCount=0

    VALUES=()

    myresult=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]//""${3}" - )
	itemsCount=$( echo "${1}" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "count(//foxml:datastream[@ID='""${2}""']/foxml:datastreamVersion[last()]//""${3}"")" - )

	if (( itemsCount != 0)) ;
	then
	    readarray -t VALUES <<<"$myresult"
	fi

    (( v >= 2 )) && printf "\n%s\n" "DATASTREAM NAME = ${2}"
    (( v >= 2 )) && printf "%s\n" "TERM NAME = ${3}"
    (( v >= 2 )) && printf "%s\n" "MYRESULT = ${myresult}"
    (( v >= 2 )) && printf "%s\n" "itemsCount = ${itemsCount}"
}

####################### FUNCTIONS - MISC STRING AND ARRAY MANIPULATION FUNCTIONS #######################
#
# GIVEN:
#   - A string intended to be a csv field value.
#
# RETURNS:
#   - An 'escaped' string acceptable as a csv field value.
#
# DESCRIPTION:
# Checks input string for characters that need special handling (comma, newlines, double quotes).
# If any of those are found, wrap entire string in double quotes and escape any double quotes already in the string.
#
function esc_csv()
{
    local myresult=''

	if [[ "$1" == *['"',$'\n']* ]] ;
	then
        myresult='"'$(sed -e 's/"/""/g' <<< "$1")'"'
    else
        myresult="$1"
    fi

    echo "${myresult}"
}

#
# GIVEN:
#   - An array of strings (metadata values).
#
# RETURNS:
#   - A single string built from the given array of strings.
#
# DESCRIPTION:
# Builds a single string put together from an array of strings.  The array of strings are metadata values.
# Puts them together, with a separator string, and escapes the result to make it acceptable as a csv field value.
#
function build_metadata_string()
{
    local arr=("$@")
    local n_elements=${#arr[@]}
    local RET_STR=''

    if (( n_elements != 0 )) ;
    then
		RET_STR=$(printf "${SEPARATOR}%s" "${arr[@]}")
		RET_STR=${RET_STR:${SEPARATORLEN}}
		RET_STR=$( esc_csv "${RET_STR}")
	fi

	echo "${RET_STR}"
}

#
# GIVEN:
#   - An array of metadata values. (array1)
#   - An array of metadata values. (array2)
#
# RETURNS:
#   - A single array of metadata values built from the given arrays (VALUES)
#
# DESCRIPTION:
# Builds a single array from the given arrays.  It does this by adding values from array 2 into array1 if they are not
# already there.
#
function build_metadata_array()
{

	local -a array1=()
	local -a array2=()
	local j

    array1=( "${@:2:$1}" ); shift "$(( $1 + 1 ))"
    array2=( "${@:2:$1}" ); shift "$(( $1 + 1 ))"
    VALUES=( "${array1[@]}" )

	for j in "${!array2[@]}"; do
		if [[ ! " ${array1[@]} " =~ " ${array2[$j]} " ]]; then
			VALUES+=( "${array2[$j]}" )
		fi
	done

    (( v >= 2 )) && printf "\n%s\n" "BUILDING METADATA ARRAY"
    (( v >= 2 )) && printf 'ARRAY1 = %s\n' "${array1[@]}"
    (( v >= 2 )) && printf 'ARRAY2 = %s\n' "${array2[@]}"
    (( v >= 2 )) && printf 'VALUES = %s\n' "${VALUES[@]}"
}

####################### MAIN FUNCTION #######################

DONE=0

while getopts "ad:efo:q:hv:" opt; do
  case ${opt} in
    a )
      APPENDARG=1
      ;;
    d )
      FILEDIR="${OPTARG}"
      ;;
    e )
      EXPEDIENTARG=1
      FILESARG=0
      ;;
    f )
      FILESARG=1
      ;;
    o )
      OUTFILE="${OPTARG}"
      ;;
    q )
      QUERYARG="${OPTARG}"
      QUERY="query=${OPTARG}"
      ;;
    v )
      VERBOSEARG="${OPTARG}"
      ;;
    h )
 	  printf "\n%s\n\n" "Usage: $(basename $0) [-q queryvalue] [-o output_file_name] [-d output_dir_path] [-f] [-v 0..2]" >&2
      exit 1
      ;;
  esac
done
shift $((OPTIND -1))

v=$VERBOSEARG
# e=$EXPEDIENTARG

(( v >= 1 )) && printf "\n***************************************************************************\n"
(( v >= 1 )) && printf "************** STARTING JOB AT: %s **************\n" "$(date)"
(( v >= 1 )) && printf "************** GETTING DATA FROM: %s **************\n" "FEDORA 3 @ DIGITAL.UNH.EDU"
(( v >= 1 )) && printf "***************************************************************************\n"

if [ ! -z "$QUERY" ]
then
	(( v >= 1 )) && printf '\n%s\n' '***************************************************************************'
	(( v >= 1 )) && printf '%s\n' "GETTING DATA FOR: $QUERY OBJECTS"
	(( v >= 1 )) && printf '%s\n\n' '***************************************************************************'
else
	(( v >= 1 )) && printf '\n%s\n' '***************************************************************************'
	(( v >= 1 )) && printf '%s\n' "GETTING DATA FOR: all OBJECTS"
	(( v >= 1 )) && printf '%s\n\n' '***************************************************************************'
fi

OUTPUT_FILE="${FILEDIR}"'/'"${OUTFILE}"

# CHECK FOR EXISTENCE OF OUT.CSV.  ASK USER TO OVERWRITE.
if [[ -f "$OUTPUT_FILE" ]]; then
    if (( APPENDARG == 0 )) ; then
	    printf "%s\n " "$OUTPUT_FILE ALREADY EXISTS. (OVERWRITING)"
	else
	    printf "%s\n " "$OUTPUT_FILE ALREADY EXISTS. (APPENDING)"
	fi
<< 'COMMENT'
    printf "%s\n " "$OUTPUT_FILE already exists."
    read -p "Do you want to overwrite it? [yYnN] " -n 1 -r
	echo    # (optional) move to a new line
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		printf "%s\n\n" "Exiting script." 1>&2
    	exit 1
    else
		printf "%s\n\n" "Continuing..." 1>&2
	fi
COMMENT
fi

# Track script execution time
begin=$(date +%s)

# CREATE FILE DIRECTORY AND FILE.
mkdir -p "${FILEDIR}" && touch "$OUTPUT_FILE"

# PRINT THE CSV FILE HEADER IF WE WANT.
if (( APPENDARG == 0 )) ; then
  printf 'state,pid,collection,dcterms:identifier,dcterms:title,dcterms:date,dcterms:description,dcterms:contributor,dcterms:rights,dcterms:format,dcterms:type,dcterms:subject,dcterms:creator,dcterms:relation,dcterms:coverage,dcterms:language,dcterms:publisher,files\n' > "${OUTPUT_FILE}"
fi

NPIDS=0
TOTPIDS=0

while [ ${DONE} == 0 ]
do
    if [ -z "$TOKEN" ]
    then
    	if [ ! -z "$QUERY" ]
    	then
			# set -x
			RESPONSE=$(curl -s --location --request GET "${GETREQUESTBASE}" -G -d "${QUERY}")
			# set +x
		else
		    printf "%s\n\n" "***NO QUERY SPECIFIED - GETTING ALL OBJECTS FROM FEDORA 3" 1>&2
			# set -x
			RESPONSE=$(curl -s --location --request GET "${GETREQUESTBASE}" -G -d 'query=pid~*')
			# set +x
		fi
	else
		# set -x
		RESPONSE=$(curl -s --location --request GET "${GETREQUESTBASE}${TOKENARG}${TOKEN}")
		# set +x
	fi

	res=$?
	if test "$res" != "0"; then
        printf "\n%s\n\n" "The curl command FAILED with: $res"  1>&2
    # else
    #     printf "%s\n\n" "The curl command SUCCEEDED with: $res"  1>&2
    fi

	if [ -z "$RESPONSE" ]
	then
		printf "%s\n\n" "***No objects retrieved. Check your command line for errors." 1>&2
		exit 1
	fi

	# Get rid of spurious tabs/spaces/linebreaks added into the response.
	RESPONSE=$( echo "${RESPONSE}" | tidy -xml -wrap 0 -q -)

	PIDS=$(echo "${RESPONSE}" | xmlstarlet sel -t -v  "/_:result/_:resultList/_:objectFields/_:pid"  -)
	NPIDS=$(echo "${RESPONSE}" | xmlstarlet sel -t -v  "count(/_:result/_:resultList/_:objectFields/_:pid)"  -)

	if [ "$NPIDS" -eq 0 ]
	then
		# First time around. Got no objects with the query
	    if (( TOTPIDS == 0 ))
	    then
	    	printf "%s\n\n" "***No objects retrieved with the query: ${QUERYARG}" 1>&2
	    	#exit 1
		fi

		# Done this time around (not the first time).  Processing the last little bit.
	    DONE=1
	else
		TOKEN=$(echo "$RESPONSE" |  xmlstarlet sel -t -v "/_:result/_:listSession/_:token" -)
		CURSOR=$(echo "$RESPONSE" |  xmlstarlet sel -t -v "/_:result/_:listSession/_:cursor" -)
		TOTPIDS=$(( TOTPIDS + NPIDS ))

		if [ -z "$TOKEN" ] && [ -z "$CURSOR" ]
		then
		  DONE=1
		fi

		(( v >= 3 )) && echo "-----------"
		(( v >= 3 )) && echo "TOKEN=${TOKEN}"
		(( v >= 3 )) && echo "CURSOR=${CURSOR}"
		(( v >= 3 )) && echo "TOTPIDS=${TOTPIDS}"
		(( v >= 3 )) && echo "NPIDS=${NPIDS}"
	    (( v >= 3 )) && echo "PIDS = ${PIDS}"
		(( v >= 3 )) && echo "-----------"
		(( v >= 3 )) && echo ""

		# GET EACH FEDORA OBJECT - IDENTIFIED BY PID. ASSEMBLE THE DATA FROM EACH OBJECT.
		while IFS= read -r line; do
		    PID="$line"
		    (( v >= 1 )) && printf "%s\n" "***Retrieving: ${PID}"

		    # Get the foxml for the object identified by pid (line)
			OBJ_FOXML=$(f3api_objectXML "$PID")

			# Tidy it up.
			OBJ_FOXML=$( echo "${OBJ_FOXML}" | tidy -xml -wrap 0 -q -)

			# Get the state of this object. (Active/Inactive/etc.?)
			STATE=$( echo "$OBJ_FOXML" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -t -v "/foxml:digitalObject/foxml:objectProperties/foxml:property[@NAME=\"info:fedora/fedora-system:def/model#state\"]/@VALUE" )

			# Get datastream count.
			NDATASTREAMS=$( echo "$OBJ_FOXML" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "count(/foxml:digitalObject/foxml:datastream)" )

			IDX=0
			FILELIST=''

			# FOR EACH DATASTREAM DO THIS.
			for (( i=1; i <= NDATASTREAMS; i++ ))
			do
				DATASTREAM_ID=$( echo "$OBJ_FOXML" | xmlstarlet sel -N foxml="${NAMESPACES[FOXML]}" -N dc="${NAMESPACES[DC]}" -t -v  "/foxml:digitalObject/foxml:datastream[${i}]/@ID" )

				# Contains dcterms (qualified dublin core)
				if [ "$DATASTREAM_ID" = "XML" ] ;
				then

					(( v >= 3 )) && printf "\n%s\n" "***GOT THE ${DATASTREAM_ID} DATA STREAM!"

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:contributor"
					DCQCONTRIBUTORS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:coverage"
					DCQCOVERAGES=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:creator"
					DCQCREATORS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:date"
					DCQDATES=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:description"
					DCQDESCRIPTIONS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:format"
					DCQFORMATS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:identifier"
					DCQIDENTIFIERS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:language"
					DCQLANGUAGES=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:publisher"
					DCQPUBLISHERS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:relation"
					DCQRELATIONS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:rights"
					DCQRIGHTS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:subject"
					DCQSUBJECTS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:title"
					DCQTITLES=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "XML" "dc:type"
					DCQTYPES=("${VALUES[@]}")

				elif [ "$DATASTREAM_ID" = "DC" ] ;
				then

					(( v >= 3 )) && printf "\n%s\n" "***GOT THE ${DATASTREAM_ID} DATA STREAM!"

					# Don't need this.
					# foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:creator"
					# DCCREATORS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:description"
					DCDESCRIPTIONS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:identifier"
					DCIDENTIFIERS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:publisher"
					DCPUBLISHERS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:subject"
					DCSUBJECTS=("${VALUES[@]}")

					foxmlExtract_Values "$OBJ_FOXML" "DC" "dc:title"
					DCTITLES=("${VALUES[@]}")

				elif [ "$DATASTREAM_ID" = "RELS-EXT" ] ;
				then

					(( v >= 3 )) && echo "***GOT THE ${DATASTREAM_ID} DATA STREAM!"
					(( v >= 3 )) && echo ""

					foxmlExtract_Collections "$OBJ_FOXML"
					COLLECTIONS=("${VALUES[@]}")

					(( v >= 3 )) && echo "COLLECTIONS_STR=${COLLECTIONS_STR}"
					(( v >= 3 )) && echo ""

				elif [ "$DATASTREAM_ID" = "AUDIT" ] ;
				then

					(( v >= 3 )) && echo "***SKIPPING THE ${DATASTREAM_ID} DATA STREAM!"
					(( v >= 3 )) && echo ""

				else

					(( v >= 3 )) && echo "***GOT THE ${DATASTREAM_ID} DATA STREAM!"
					(( v >= 3 )) && echo ""

					MIMETYPE=$( foxmlExtract_ContentMimeType "$OBJ_FOXML" "$DATASTREAM_ID" )

					if [ ! -z "$MIMETYPE" ];
					then
						if [ "$MIMETYPE" = 'image/tiff' ] || [ "$MIMETYPE" = 'image/jpeg' ]  || [ "$MIMETYPE" = 'image/jpg' ] || [ "$MIMETYPE" = 'application/pdf' ] || [ "$MIMETYPE" = 'video/mpeg' ] || [ "$MIMETYPE" = 'application/x-shockwave-flash' ] ;
						then
							(( v >= 3 )) && echo ""
							(( v >= 3 )) && echo "************************************************************************************"
							(( v >= 3 )) && echo "***** FOUND ${DATASTREAM_ID} DATASTREAM IN OBJECT WITH PID ${PID}"

							LABEL=$( foxmlExtract_ContentLabel "$OBJ_FOXML" "$DATASTREAM_ID" )
							INTERNAL_ID=$( foxmlExtract_ContentInternalID "$OBJ_FOXML" "$DATASTREAM_ID" )
							CLTYPE=$( foxmlExtract_ContentType "$OBJ_FOXML" "$DATASTREAM_ID" )

							FILENAMEBASE=''
							if [ "$CLTYPE" = "INTERNAL_ID" ] ;
							then
								FILENAMEBASE="$INTERNAL_ID"
							elif [ "$CLTYPE" = "URL" ] ;
							then
								URL=$( foxmlExtract_ContentUrl "$OBJ_FOXML" "$DATASTREAM_ID" )
								FILENAMEBASE=$( basename "$URL" )
							fi

							# Create a file name from the desired quantity extracted from the datastream.
							FILENAME=$( echo "${FILENAMEBASE}" | tr "[:]" "_" | tr [:upper:] [:lower:])

							if (( EXPEDIENTARG == 1 )) ;
							then
							    CODE=200
							elif [[ -f "${FILEDIR}/${FILENAME}" ]] ;
							then
							    CODE=201
							else
							    CODE=$( f3api_objectCode "$PID" "$DATASTREAM_ID" )
							fi

							POSSDOWNLOADS="$(( POSSDOWNLOADS+1 ))"

							if [ "$CODE" -eq 200 ] && [ ! -z "$FILENAME" ] ; then
								if [ -z "$FILELIST" ] ; then
									FILELIST="${FILENAME}"
								else
									FILELIST="${FILELIST}${SEPARATOR}${FILENAME}"
								fi

								(( v >= 3 )) && echo ""
								(( v >= 3 )) && echo "***** SOME DATASTREAM VARIABLES *****"
								# (( v >= 3 )) && echo "DATASTREAM=${DATASTREAM}"
								(( v >= 3 )) && echo "DATASTREAM_ID=${DATASTREAM_ID}"
								(( v >= 3 )) && echo "LABEL=${LABEL}"
								(( v >= 3 )) && echo "INTERNAL_ID=${INTERNAL_ID}"
								(( v >= 3 )) && echo "URL=${URL}"
								(( v >= 3 )) && echo "MIMETYPE=${MIMETYPE}"
								(( v >= 3 )) && echo "FILENAMEBASE=${FILENAMEBASE}"
								(( v >= 3 )) && echo "FILENAME=${FILENAME}"
								(( v >= 3 )) && echo "CLTYPE=${CLTYPE}"
								(( v >= 3 )) && echo "CODE=${CODE}"

								# Get the foxml for the object identified by pid (line)
								(( v >= 3 )) && echo ""
								(( v >= 3 )) && echo "***** DOWNLOADING THE DATASTREAM FILE *****"

								if (( FILESARG == 1 ))  ; then
									(( v >= 1 )) && printf "%s" "Downloading file to: ${FILEDIR}/${FILENAME}"

									f3api_objectContent "$PID" "$DATASTREAM_ID" "$FILENAME" "$FILEDIR"

									TOTDOWNLOADS="$(( TOTDOWNLOADS+1 ))"
								fi
							elif [ "$CODE" -eq 201 ] ;
							then
								if [ -z "$FILELIST" ] ; then
									FILELIST="${FILENAME}"
								else
									FILELIST="${FILELIST}${SEPARATOR}${FILENAME}"
								fi
								(( v >= 1 )) && printf "%s\n" "ALREADY DOWNLOADED: ${FILEDIR}/${FILENAME}.  SKIPPING."
							    SKIPPEDDOWNLOADS="$(( SKIPPEDDOWNLOADS+1 ))"
							else
								#KEY=$( echo "${PID}_${IDX}" | tr "[:]" "_" )
								KEY="${PID}-${IDX}"
								(( v >= 3 )) && echo "KEY=$KEY"
								(( v >= 3 )) && echo ""
								DOWNLOADERRS["${KEY}"]="$DATASTREAM_ID"
								IDX="$(( IDX+1 ))"

								(( v >= 1 )) && printf "%s\n" "Error downloading: ${FILEDIR}/${FILENAME}"
							fi

						fi
					else
						(( v >= 3 )) && echo "SKIPPING FILE DOWNLOAD."
						(( v >= 3 )) && echo ""
					fi

				fi

			done

			build_metadata_array \
			  "${#DCQCREATORS[@]}" "${DCQCREATORS[@]}" \
			  "${#DCCREATORS[@]}" "${DCCREATORS[@]}"
			DCQCREATORS=("${VALUES[@]}")

			build_metadata_array \
			  "${#DCQDESCRIPTIONS[@]}" "${DCQDESCRIPTIONS[@]}" \
			  "${#DCDESCRIPTIONS[@]}" "${DCDESCRIPTIONS[@]}"
			DCQDESCRIPTIONS=("${VALUES[@]}")

			build_metadata_array \
			  "${#DCQIDENTIFIERS[@]}" "${DCQIDENTIFIERS[@]}" \
			  "${#DCIDENTIFIERS[@]}" "${DCIDENTIFIERS[@]}"
			DCQIDENTIFIERS=("${VALUES[@]}")

			build_metadata_array \
			  "${#DCQPUBLISHERS[@]}" "${DCQPUBLISHERS[@]}" \
			  "${#DCPUBLISHERS[@]}" "${DCPUBLISHERS[@]}"
			DCQPUBLISHERS=("${VALUES[@]}")

			build_metadata_array \
			  "${#DCQSUBJECTS[@]}" "${DCQSUBJECTS[@]}" \
			  "${#DCSUBJECTS[@]}" "${DCSUBJECTS[@]}"
			DCQSUBJECTS=("${VALUES[@]}")

			build_metadata_array \
			  "${#DCQTITLES[@]}" "${DCQTITLES[@]}" \
			  "${#DCTITLES[@]}" "${DCTITLES[@]}"
			DCQTITLES=("${VALUES[@]}")

			DCQCONTRIBUTORS_STR=$( build_metadata_string "${DCQCONTRIBUTORS[@]}" )
			DCQCOVERAGES_STR=$( build_metadata_string "${DCQCOVERAGES[@]}" )
			DCQCREATORS_STR=$( build_metadata_string "${DCQCREATORS[@]}" )
			DCQDATES_STR=$( build_metadata_string "${DCQDATES[@]}" )
			DCQDESCRIPTIONS_STR=$( build_metadata_string "${DCQDESCRIPTIONS[@]}" )
			DCQFORMATS_STR=$( build_metadata_string "${DCQFORMATS[@]}" )
			DCQIDENTIFIERS_STR=$( build_metadata_string "${DCQIDENTIFIERS[@]}" )
			DCQLANGUAGES_STR=$( build_metadata_string "${DCQLANGUAGES[@]}" )
			DCQPUBLISHERS_STR=$( build_metadata_string "${DCQPUBLISHERS[@]}" )
			DCQRELATIONS_STR=$( build_metadata_string "${DCQRELATIONS[@]}" )
			DCQRIGHTS_STR=$( build_metadata_string "${DCQRIGHTS[@]}" )
			DCQSUBJECTS_STR=$( build_metadata_string "${DCQSUBJECTS[@]}" )
			DCQTITLES_STR=$( build_metadata_string "${DCQTITLES[@]}" )
			DCQTYPES_STR=$( build_metadata_string "${DCQTYPES[@]}" )

			COLLECTIONS_STR=$( build_metadata_string "${COLLECTIONS[@]}" )

			# PRINT CSV RECORD
			if [ ! -z "$PID" ] ;
			then
				(( v >= 1 )) && printf "%s\n" "Writing metadata record to: ${FILEDIR}/${OUTFILE}"

				printf '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n' "$STATE" "$PID" "$COLLECTIONS_STR" "$DCQIDENTIFIERS_STR" "$DCQTITLES_STR" "$DCQDATES_STR" "$DCQDESCRIPTIONS_STR" "$DCQCONTRIBUTORS_STR" "$DCQRIGHTS_STR" "$DCQFORMATS_STR" "$DCQTYPES_STR" "$DCQSUBJECTS_STR" "$DCQCREATORS_STR" "$DCQRELATIONS_STR" "$DCQCOVERAGES_STR" "$DCQLANGUAGES_STR" "$DCQPUBLISHERS_STR" "$FILELIST" >> "${OUTPUT_FILE}"
			fi

        done <<< "$PIDS"
	fi

	#DONE=1
done

# Track script execution time.
end=$(date +%s)
duration=$(expr "$end" - "$begin")

# SCRIPT_DIR="$( cd "$( dirname "$0" )" && pwd )"

# Print some script stats.
(( v >= 1 )) && printf "\n%s\n" "******************************************************************"
(( v >= 1 )) && printf "SCRIPT: %s\n" `realpath $0`
(( v >= 1 )) && printf "HOST: %s\n" `hostname`
(( v >= 1 )) && printf "SCRIPT RUN TIME = %.2f seconds (%.2f minutes)(%.4f hours)\n" "$duration" $(echo "$duration"/60 | bc -l) $(echo "$duration"/3600 | bc -l)
(( v >= 1 )) && printf "NUMBER OF FEDORA OBJECTS RETRIEVED = %s\n" $TOTPIDS
(( v >= 1 )) && printf "OUTPUT FILE: %s/%s\n" "$FILEDIR" "$OUTFILE"
(( v >= 1 )) && printf "DOWNLOAD DIR: %s\n" "$FILEDIR"
(( v >= 1 )) && printf "FEDORA 3 SERVER: digital.unh.edu:8080\n"
if (( FILESARG == 1 ))  ;
then
	(( v >= 1 )) && printf "OPTED FOR FILE DOWNLOADS.\n"
	(( v >= 1 )) && printf "POSSDOWNLOADS = %s\n" $POSSDOWNLOADS
	(( v >= 1 )) && printf "ACTUALDOWNLOADS = %s\n" $TOTDOWNLOADS
	(( v >= 1 )) && printf "SKIPPEDDOWNLOADS = %s\n" $SKIPPEDDOWNLOADS
	(( v >= 1 )) && printf "DOWNLOADERRS:\n"
	for index in ${!DOWNLOADERRS[*]}
	do
		(( v >= 1 )) && printf "  %s: %s\n" "${index}" "${DOWNLOADERRS[$index]}"
	done
else
	if (( EXPEDIENTARG == 1 )) ;
	then
		(( v >= 1 )) && printf "OPTED FOR NO FILE DOWNLOADS. (EXPEDIENT)\n"
	else
		(( v >= 1 )) && printf "OPTED FOR NO FILE DOWNLOADS.\n"
	fi
fi
#for item in ${DOWNLOADERRS[*]}
#do
#    printf "   %s\n" $item
#done
(( v >= 1 )) && printf "%s\n" "******************************************************************"

(( v >= 1 )) && printf "\n***************************************************************************\n"
(( v >= 1 )) && printf "*************** ENDING JOB AT: %s ***************\n" "$(date)"
(( v >= 1 )) && printf "***************************************************************************\n\n"

