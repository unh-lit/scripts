#!/bin/bash
#
# Only works in bash.  Sample commands:
# ./export-pid.sh ./fedora3-collections-c.txt
# - OR -
# ./export-pid.sh ./fedora3-collections-c.txt > output-c.txt 2>&1 &
# tail -f output-c.txt
#
# Where fedora3-collections-c.txt lists lines with the following elements (#=line is commented out):
#
# PID      DIRECTORY  CSVFILE
# court:2* court      court1
#
grep -v '^#' < "$1" | { while read line; do vars=( $line ) ; ./f3export.sh -q "pid~${vars[0]}"  -f -d "/cygdrive/d/FedoraStorage/collections/${vars[1]}" -o "${vars[2]}.csv" ; done; }
# grep -v '^#' < "$1" | { while read line; do vars=( $line ) ; ./f3export.sh -q "pid~${vars[0]}"  -f -d "./tmp/collections/${vars[1]}" -o "${vars[2]}.csv" ; done; }
# grep -v '^#' < "$1" | { while read line; do vars=( $line ) ; ./f3export.sh -q "pid~${vars[0]}"  -f -d "/Volumes/FedoraStorage/collections/${vars[1]}" -o "${vars[2]}.csv" ; done; }
