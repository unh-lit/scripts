# F3EXPORT SCRIPT

This script retrieves the metadata and files for specified objects in the Fedora 3 digital repository at digital.unh.edu.
By default, object metadata and files are downloaded to the ./tmp directory which can be found within the script directory.
Additionally, the metadata is deposited into the out.csv file in the ./tmp directory.

## Export Process/Quick Start

We will be using the litpgfs server to run this script.  If your account and all required tools have been set up for you on that server, you can just run the 
script as described in this section.

- Use the Microsoft Remote Desktop app (RDP) to log into litpgfs from either the 
UNH wired network or the VPN if you are on the UNH secure wireless network.

- Open a Cygwin window.

- Change directories to the f3export script directory:

>    cd ~/git/scripts/f3export

- Run the script from that directory, as described in 'Sample Commands'.

- The default output file for metadata is ./tmp/out.csv.  The default file download directory is ./tmp.

- The script will display progress in the Cygwin window, but if you are interested in monitoring file downloads directly
you can do so by opening the tmp folder from Windows Explorer.

- There will be storage issues if you are downloading large collections onto the litpgfs C drive. However, litpgs
will have a large D drive soon.  When that happens, you can use the command options to route metadata and other
downloaded files to directories on the D drive.  

- The other option to mitigate storage issues is to mount your Box drive and use Windows Explorer
to move data to your Box drive after running the script.

### Sample Commands

In the sample commands, below, 'collection-name' in the query (-q) option should always be replaced by a Fedora 3 collection name.  
There is a list of Fedora 3 collection names [here](https://gitlab.com/unh-lit/scripts/-/blob/master/f3export/fedora3-collections.txt).

Wildcards are accepted in the query option. '*' matches zero or more of any character. '?' matches exactly one of any character. 

'NNNN' in a sample command query represents a 4-digit number and is always replaced by one when typing a command. Acceptable values are things like '0001' or
'9876'.

To retrieve an entire collection's metadata including all associated files from the Fedora 3 server, run the following command in your Cygwin window:

>    ./f3export.sh -q "pid~collection-name:*" -f

To retrieve only the collection's metadata, type the following in your Cygwin window:

>    ./f3export.sh -q "pid~collection-name:*"

To retrieve only metadata for a single object, type the following into your Cygwin window, replacing the NNNN with the number that matches the object's pid.
 
>    ./f3export.sh -q "pid~collection-name:NNNN"

To retrieve metadata and associated files for a single object, type the following into your Cygwin window:

>    ./f3export.sh -q "pid~collection-name:NNNN" -f

To retrieve a collection, altering the download directory and/or changing the name of the csv file:

>    ./f3export.sh -q "pid~collection-name:*" -o output_file_name -d output_directory_path

Output_file_name can be anything with a .csv suffix. For example, "collection-name.csv" is acceptable.

Output_directory_path can be any pathname to a folder where you have write access.
For example, if you want to put data into a folder on the D: drive, you could 
specify something like "/cygdrive/d/fedora_data/collection_name".
The "fedora_data" directory must exist and you must have access to write to that directory.

With no arguments, this script will retrieve the metadata for all objects in the Fedora 3 Repository at digital.unh.edu
and deposit their metadata into the out.csv file in the ./tmp directory:

>    ./f3export.sh

Note that files are NOT retrieved by default.  They will only be downloaded if the -f option is specified in the command line.

Also note that trying to download the entire repository is not the recommended way of running the script.  It is very slow and may take days to extract 
the metadata and files for the entire repository.  Additionally, if you don't change the download directory, you will have storage isses.

## Script Requirements

The following requirements must be met to run the script on a particular machine:

- You must have access to and be logged into the UNH VPN.
- Tidy must be installed and in your search paths. ('yum install tidy' on linux or 'brew install tidy' on mac os x)
- Bash >=v4.0 must be installed and must be your current shell.
- Curl must be installed and in your search paths. ('yum install curl' on linux or 'brew install curl' on mac os x)
- Xmlstarlet must be installed and in your search paths (add 'extras' RHEL repo then 'yum install xmlstarlet' on linux or 'brew install xmlstarlet' on mac os x)
- Output_dir_path must exist and the user must have 'w' access.
- On Windows, Cygwin must be installed prior to installing other required software.  The other required packages can be installed at the same time CygWin is installed.
- On Mac OS, Brew is the suggested way of installing other required software.
- On Linux, yum can be used to installed required software.

## Script Setup

You should only have to do this once.

- Set up all script requirements on the computer where the script will be run.

- Open a terminal window on the computer where you are running this script. (Either ssh to a linux machine, open a Cygwin window on Windows 10 or Windows Server, or open a terminal window on Mac OS X.)

- Create a directory to contain the scripts repository.  I usually create a directory named 'git' from my home directory.
 
>    cd ~; mkdir git ; cd git

- Clone the scripts repository from gitlab to the directory you just created.

>    git clone https://@gitlab.com:unh-lit/scripts.git

- Make sure you move to the directory containing the f3export script:

>    cd ~/git/scripts/f3export

- Run the script commands as described in 'Quick Start'.

## Script Updates

Sometimes the script code is updated in the repository.  This is to add new features or to fix bugs. In this case you should update your repository as follows.

- Open a terminal window on the computer 
where you are running this script. (Either ssh to a linux machine, open a Cygwin window on Windows 10 or Windows Server, or open a terminal window on Mac OS X.)

- Execute the following commands to pull changes from the gitlab repository to your machine.

>    cd ~/git/scripts<br>
>    git fetch all<br>
>    git pull origin master

## Script Usage and Options

USAGE:

>    ./f3export.sh [-q query_string] [-o output_file_name] [-d output_dir_path] [-f] [-v [0-2+]]


OPTIONS:

- -h  
Prints a usage message and quits.

- -d output_dir_path  
Directory to which object files are exported, including the out.csv file  Currently only files with mimetype tiff, jpeg, pdf are exported. 
This can be expanded. (Default: ./tmp.)

- -f  
Turns on file downloading. With this option, if a fedora object has files associated with it, they are downloaded to the output_dir_path (-d). (Default: off.)

- -o output_file_name  
The csv output file path. (Default: out.csv.)

- -q query_string  
A query to specify which objects to extract from the Fedora 3 repository.  The default is no query which will pull
all objects from the Fedora 3 repository, 20 at a time.  This can take quite a long time.
A good description of allowable queries is at http://digital.unh.edu:8080/fedora/objects, click on either of the 'help' links.
(Default: no query string which means 'pull all objects from the repository'.)

- -v  
verbosity level - How much output you see from the script.  0 = silent.  1 = some output (default). 2+ = verbose (debug).

- -r  
(Coming soon.) Run in reporting mode. Only audit trail reporting for now.

## Notes

You must be logged into the UNH VPN or on the wired UNH network for this script to work.

Remember to run this script from its root directory so it can access to the ./tmp directory for outputting the CSV and other files if defaults are uses.

Files are NOT downloaded by default. Only the csv file is generated.  To download files, use the -f option.

This script runs very SLOWLY, so be patient.  However, you should see output pretty much right away.

There are storage concerns for large collections.

## More Sample Commands

>    ./f3export.sh -h<br>
>    ./f3export.sh -q "pid~a*"<br>
>    ./f3export.sh -q "pid~demo*"<br>
>    ./f3export.sh -q "pid~moran:0195"<br>
>    ./f3export.sh -q "pid~acworth:0001"<br>
>    ./f3export.sh -q "pid~acworth:000*"<br>
>    ./f3export.sh -q "pid~demo:5"<br>
>    ./f3export.sh -q "pid~moran:0195" -o output_file_name -d output_dir_path -f<br>
>    ./f3export.sh -q "pid~morann:0195"<br>
>    ./f3export.sh -q "pid~acworth:*" -f -d ./tmp/fedora3-data-exports/acworth<br>
>    ./f3export.sh -q "pid~demo:SmileyTallRoundCup" -f<br>

## Informative URLS

The following URLS give web access to the Fedora3 repository UI:

- http://digital.unh.edu:8080/fedora/describe
- http://digital.unh.edu:8080/fedora/objects

Currently this script only accesses the Fedora 3 repository at http://digital.unh.edu:8080, but
arguments will be added to allow this to access any Fedora 3 repository given a domain, port, and Fedora base (default is 'fedora').

## Fedora 3 Collections List

For informational purposes and for use with the script, there is list of collections contained in our Fedora 3 repository [here](https://gitlab.com/unh-lit/scripts/-/blob/master/f3export/fedora3-collections.txt).

## Simple Bash Commands

Some descriptions can be found [here](https://www.educative.io/blog/bash-shell-command-cheat-sheet).

>    \# Some useful bash commands:  
>  
>    \# List the contents of the current directory (folder).  
>    ls -al  
>  
>    \# Print the path of the current directory (folder).  
>    pwd -P  
>   
>    \# Change your current working directory to your home directory ('~' = home dir, '..' = up one level, '.' = current dir).  
>    cd ~  
>  
>    \# Go up one directory level.  
>    cd ..  
>  
>    \# Change your current working directory to the f3export script directory.  
>    cd ~/scripts/git/f3export  

